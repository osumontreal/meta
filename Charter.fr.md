﻿# Open Science UMontreal

Mon campus pour une science ouverte

## Charte

## 0. Dénomination

Open Science UMontreal

## 1. Siège social

Université de Montréal, C.P. 6128, succursale Centre-Ville, Montréal, QC

## 2. Mission

Open Science UMontréal - Mon campus pour la science ouverte a pour
mission de promouvoir une science au service du bien commun, où la société a
accès librement aux processus de création, aux contenus et retombées de la
recherche scientifique en plus de défendre les principes d’un monde de recherche
plus inclusif, collaboratif et transparent. Nous croyons que cette ouverture
encouragera une plus grande collaboration nous permettant de résoudre plus
efficacement les problèmes auxquels nous sommes et serons confrontés.

## 3. Objectifs

* Créer un espace pour les membres de la communauté dans le but d’échanger sur
  les enjeux liés à une science plus ouverte, qui respectent les principes
  d’équité, de diversité et d’inclusion;
* Promouvoir une pratique ouverte auprès des différentes instances
  institutionnelles ainsi que la communauté estudiantine de l’Université;
* Proposer différentes opportunités d’apprentissage par le biais d’ateliers et
  d’événements favorisant le développement des compétences liées à la science
  ouverte;
* Instaurer un réseau collaboratif et transdisciplinaire entre les communautés
  scientifiques universitaires locales.

## 4. Membres

### 4.1 Statut

Toute personne physique est considérée membre si :

* Elle est étudiante, pour l’année en cours, à l’Université de Montréal;
* Elle est inscrite sur la liste des membres;
* Adhère au code de conduite du regroupement (voir Annexe I).

Une personne réputée membre a le droit :

* de participer à toutes les activités;
* recevoir les avis de convocation aux assemblées des membres, d'assister à ces assemblées et d'y voter;
* est éligible pour un des postes disponibles dans l’exécutif du regroupement.

### 4.2 Suspension et expulsion

Tout membre qui commet un écart au code de conduite peut se voir suspendu ou
expulsé du groupe par le comité exécutif. Aucune tolérance déviant de ce qui est
accepté ne sera permise.

## 5. Organismes

L'Assemblée générale est composée des membres du regroupement.

Le comité exécutif est composé des membres élu·e·s à l’assemblée générale
affecté·e·s à une fonction de direction.

Des comités spontanés peuvent être formés. Leur existence doit être sanctionnée
par l’assemblée générale. Les actions des comités sont réputées des actions du
regroupement tant qu’elles respectent sa charte et ses positions votées en
Assemblée générale.

## 6. Assemblée générale

L’Assemblée générale vote les décisions et les positions du regroupement. Elle
élit et destitue les membres formant le comité exécutif de l’association. Elle
est l’instance suprême et donc peut révoquer toute décision du comité exécutif
ou de tout autre sous-comité si ces décisions sont jugées comme allant à
l’encontre des règlements du regroupement. Elle est convoquée dans un délai
minimal de sept (7) jours.

### 6.1 Assemblée générale spéciale

Assemblée provoquée en cas de nécessité de prise d’une décision majeure ne
pouvant attendre l’assemblée générale ordinaire. Elle peut être convoquée par le
comité exécutif ou par une pétition de 10% des membres du regroupement.

### 6.2 Quorum

10 % de l’ensemble des membres dont 3 membres sont représentant.e.s du comité
exécutif.

### 6.3 Votation

Le vote est encadré par les règlements suivants :

* seuls les membres ont droit de vote;
* une voix par membre;
* une décision devient majoritaire si elle est votée à 50 % + 1;
* tel qu’inscrit à l’article 13, une modification de la Charte doit se faire au
  2/3 des membres présent·e·s et votant·e·s;
* seuls les membres présent·e·s ont droit de vote. Il n’y a donc aucun vote par
  procuration.

## 7. Cotisation

Aucune cotisation ne sera perçue auprès des membres.

## 8. Comité exécutif

Il faut être un ou une membre active élue pour occuper des fonctions de direction.

Le comité exécutif s’occupe de la gérance du regroupement, par la prise de
décisions et leur mise en oeuvre.

Il y a un minimum d’une réunion obligatoire par mois dont le quorum est de
50% + 1 des membres élues. S’il le juge nécessaire, le comité exécutif peut, par
un vote à l’unanimité, destituer un·e membre du comité exécutif qui s’absente
sans raison exceptionnelle à deux réunions consécutives.

### 8.1 Rémunération

Tous les membres du Conseil siègent sans rémunération, mais ils et elles peuvent
se faire rembourser des dépenses raisonnablement engagées.

### 8.2 Changement d’un·e membre du comité exécutif

Les mécanismes de changement de membre de l’exécutif peuvent prendre différentes
formes telles :

* l’Assemblée générale d’élection;
* la démission;
* l’élection par l’Assemblée générale extraordinaire;
* l’expulsion par vote de confiance en Assemblée générale échoué suivi d’une
  élection.

Le Conseil exécutif doit appeler, lorsqu’il le jugera approprié, une élection
pour un poste vacant dans le cadre d’une Assemblée générale ordinaire ou
extraordinaire.

Le Conseil exécutif peut nommer, s’il le juge nécessaire, un ou une responsable
par intérim pour le poste vacant en attendant que soit élue un ou une membre à
ce poste.

Les élections sont régies à l’article 10.1 de la présente Charte.

### 8.3 Vote de confiance

Si un ou une membre du comité exécutif ne remplit pas les conditions de l’article
4.2 ou les tâches liés au rôle qu’il ou elle occupe sur le comité exécutif, un
vote de confiance peut être mené lors d’une Assemblée Générale extraordinaire.

Le vote de confiance destitue le ou la membre du comité exécutif si le 2/3 des
votant·e·s ne donne pas leur confiance au dit membre.

### 8.4 Démission

Une membre du Comité Exécutif peut volontairement démissionner s’il ou elle ne
peut plus assurer ses fonctions, permettant le bon fonctionnement du CE et du
regroupement.

### 8.5 Postes du comité exécutif

Les membres du comité exécutif sont conjointement responsables :

* d’assurer une gestion financière éthique des fonds de l'Organisation ;
* coordonner l'ensemble des événements et projets organisés par le regroupement
* prendre l'initiative de convoquer les réunions requises du conseil
  d'administration, des comités de l'événement et de l'assemblée annuelle.
* d’être signataires du regroupement notamment auprès de toute banque ou
  institution financière que les Responsables auront désignée.

#### 8.5.1 Coordination

Deux responsables sont nommé·e·s à la coordination dont un·e de premier cycle et
un·e des cycles supérieurs si possible.

Les responsables à la coordination :

* sont les représentantes officielles et les porte-paroles du regroupement;
* coordonnent le travail des Responsables et les assistent au besoin;
* voient au bon fonctionnement général des affaires du regroupement, de
  l'élaboration, du respect et de la promotion des politiques générales du
  regroupement;
* représentent le regroupement lors de réunions obligatoires dans les différentes
  instances de l’Université de Montréal;
* remplissent les fonctions d’un poste vacant, le cas échéant;
* assistent les autres membres du comité exécutif au besoin.

#### 8.5.2 Secrétariat

Le ou la responsable du secrétariat :

* assure le respect intégral de la charte et des règlements, du registre des
  membres et de la correspondance;
* convoque les membres aux assemblées générales et spéciales ainsi qu'à toute
  autre réunion;
* a la garde également des archives;
* est chargée de la rédaction et diffusion des ordres du jour et des
  procès-verbaux.

#### 8.5.3 Trésorerie

Le ou la responsable de la trésorerie :

* se charge et à la garde des fonds du regroupement et de ses livres de
  comptabilité;
* assume la préparation du budget annuel et applique les contrôles;
* signe toutes les transactions financières conjointement avec le ou la
  responsable générale du regroupement;
* produit les états financiers et les déposer en assemblée générale.

#### 8.5.4 Communications

Le ou la responsable aux communications :

* S’occupe de répondre au courriel envoyé à l’adresse principale du regroupement
* Voit à la promotion du regroupement et de ses activités
* Gère les différentes plateformes de communication (Site Web, réseaux sociaux,
  etc).

### 8.5 Transition du comité exécutif

Suite aux élections, le comité exécutif reste en place et poursuit son mandat
pour une période transitoire d’un mois.

Suite à une élection dans une assemblée générale extraordinaire d’élection, une
période transitoire d’un mois est également de mise, si en permettent les
circonstances.

La période transitoire permet aux nouveaux membres élu·e·s dans l’exécutif de
travailler conjointement avec les membres sortant·e·s du comité exécutif
précédent afin d’être formé·e·s et préparé·e·s pour l’année suivante.

## 9. Comités et sous-comités

Assemblée générale peut créer des comités au besoin.

## 10. Élections

Annuelles à main levée, sauf si le vote secret a été demandé par un·e membre, à
l’occasion de l’assemblée générale en début de l'année scolaire pour élire les
membres constituant le comité exécutif.

Toute personne membre peut présenter sa candidature pour tous les postes. La
candidature des membres sera annoncée avant les votes et les votes se dérouleront
selon l’ordre de présentation de l’article 10.1 de la charte.

### 10.1 Procédure d’élection

Les membres voteront "oui", "non" ou "abstention" pour chacun des candidat·e·s
à tour de rôle. Pour être élu·e, un candidat·e doit recevoir (ou amasser) au
minimum la majorité des votes. Si plus de cinq personnes se présentent, les
candidat·e·s ayant obtenu le plus grand pourcentage de *oui* seront élu·e·s.

Si le vote secret est demandé, la procédure suivante doit être suivie: chaque
membre doit indiquer "oui", "non" ou ne rien mettre en cas d'abstention à côté
de chaque candidat·e sur un bout de papier.

## 11. Dispositions financières

L'exercice financier se termine le 31 août de chaque année.

Les institutions financières avec lesquelles le regroupement fait affaire sont
désignées par le Comité exécutif.

Toute transaction financière doit être signée conjointement par deux membres du
comité exécutif, dont l'un des deux devant être un ou une responsable à la
coordination, le ou la secrétaire ou; le ou la trésorière.

## 12. Statut du regroupement

Regroupement étudiant à but non lucratif

## 13. Modifications de la charte

Toute modification au présent document doivent être adoptées par les 2/3 des
membres présents à l’assemblée générale.

## 14. Dissolution du regroupement

La dissolution doit être votée par les 2/3 des membres présents à l’assemblée
générale.

## 15. Adoption de la charte

Charte adoptée en assemblée générale à Montréal le 27 février 2019.

Annexe I - [Code de conduite du regroupement](Code-of-Conduct.fr.md)
